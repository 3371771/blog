<?php
/**
 * Created by PhpStorm.
 * User: xenia
 * Date: 03.05.17
 * Time: 14:06
 */

namespace app\controllers;

use app\models\BlogPost;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;


class BlogController extends Controller
{
    public function actionIndex()
    {
        $provider = new ActiveDataProvider([
           'query' => BlogPost::find(),
           'pagination' => [
                'pageSize' => 20,
                ],
        ]);

        return $this->render('index', ['dataProvider' => $provider]);

    }

    public function actionUpdate($id = null)
    {
        if ($id !== null) {
            $model = BlogPost::findOne($id);
        } else {
            $model = new BlogPost();
        }
        if ($model->load(\Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->save();
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', ['model' => $model]);
    }

    public function actionDelete($id)
    {
        $post = BlogPost::findOne($id);
        $post->delete();

       return $this->redirect(['index']);
    }

}