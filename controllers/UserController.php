<?php
/**
 * Created by PhpStorm.
 * User: xenia
 * Date: 05.05.17
 * Time: 12:54
 */

namespace app\controllers;
use app\models\Login;
use Yii;
use app\models\Reg;
use yii\web\Controller;


class UserController extends Controller
{
    public function actionReg()
    {
        $model = new Reg();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->reg()) {
                // if (Yii::$app->getUser()->login($user)) {
                return $this->goHome();
            }
        }
        // }

        return $this->render('reg', [
            'model' => $model,
        ]);
    }

    public function actionLogin()
    {
        $model = new Login();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                return '1';

            }

        }
        return $this->render('login', [
            'model' => $model,
        ]);

    }
}
