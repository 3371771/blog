<?php

use yii\widgets\ListView;
use yii\helpers\Html;
?>

<div class="button">

<?= Html::a('Добавить',['update'], ['class' => 'btn btn-primary'])?>

</div>
<?php
echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_post',
]);
