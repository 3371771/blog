<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="post">

    <h2><?= Html::encode($model->title) ?></h2>
    <?= HtmlPurifier::process($model->content) ?>
</div>

<div class="button">
    <?= Html::a('Удалить',['delete','id'=> $model->id], ['class' => 'btn btn-primary']) ?>
    <?= Html::a('Изменить',['update','id'=> $model->id], ['class' => 'btn btn-primary']) ?>
</div>