<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="form">
    <?php
        $form = ActiveForm::begin([
            'id' =>'update',
            'method'=>'post',
            'options' => ['class' => 'form-horizontal'],
        ])
    ?>

<?= $form->field($model, 'title')->textInput()->hint('Пожалуйста, введите название статьи') ?>
<?= $form->field($model, 'content')->textInput()->hint('Пожалуйста, введите текст статьи') ?>

<div class="form-group">
    <div class="button">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary']);
        ?>
    </div>
</div>
<?php ActiveForm::end() ?>
</div>