<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="form">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login',
        'options' => ['class' => 'form-horizontal'],
    ])
    ?>

    <?= $form->field($model, 'userName') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Войти', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php ActiveForm::end() ?>
</div>
