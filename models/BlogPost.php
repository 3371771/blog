<?php
/**
 * Created by PhpStorm.
 * User: xenia
 * Date: 04.05.17
 * Time: 10:58
 */

namespace app\models;
use yii\db\ActiveRecord;

class BlogPost extends ActiveRecord
{
    public function rules()
    {
        return [
            [['title', 'content'], 'required', 'message'=> 'Это поле обязательно!']
        ];
    }

    public static function tableName()
    {
        return 'blogpost';
    }

}