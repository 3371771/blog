<?php
/**
 * Created by PhpStorm.
 * User: xenia
 * Date: 05.05.17
 * Time: 13:28
 */

namespace app\models;

use yii\base\Model;


class Reg extends Model
{

    public $userName;
    public $email;
    public $password;

    public function rules()
    {
        return [
            [['email', 'password', 'userName'], 'required','message' => 'Это поле необходимо заполнить'],

            ['email', 'email', 'message'=> 'Неверный адрес почты'],
            ['password','string','min'=> 8,'message'=> 'Пароль должени быть не менее 8 символов' ]

        ];
    }

    public static function tableName()
    {
        return 'users';
    }

    public function reg()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new Users();
        $user->userName = $this->userName;
        $user->email = $this->email;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        return $user->save();
    }

}